# Fedora Sway Atomic configuration files

- terminal emulator
  - foot
  - kitty

- Command line
  - bash
  - [helix](https://github.com/helix-editor/helix/releases)
  - [kakoune](https://kakoune.org/)
  - tmux
  - [fzf](https://github.com/junegunn/fzf)
  - ripgrep
  - fd
  - [starship prompt](https://starship.rs/)
  - [wl-mirror](https://github.com/Ferdi265/wl-mirror)

- GUI
  - sway
  - waybar
  - rofi-wayland
  - [wlogout](https://github.com/ArtsyMacaw/wlogout)
  - [Zen Browser](https://zen-browser.app/) (install with flatpak)
  - [Obsidian](https://obsidian.md/) (install with flatpak)

- Theme and fonts
  - [catppuccin](https://github.com/catppuccin/catppuccin)
  - [adw-gtk3](https://github.com/lassekongo83/adw-gtk3)
  - [tela icons](https://github.com/vinceliuice/Tela-icon-theme)
  - fonts 
    - [sn-pro](https://github.com/supernotes/sn-pro) 
    - [inter](https://rsms.me/inter/) 
    - [maple mono](https://github.com/subframe7536/Maple-font)

## Install Fedora overlays

```bash
rpm-ostree install ripgrep fd-find tmux wl-mirror wlogout fzf helix kakoune
```

## Install flatpaks

### add flathub as a remote

```bash
flatpak remote-add --if-not-exists flathub https://dl.flathub.org/repo/flathub.flatpakrepo
```

### install applications and adw theme

```bash
flatpak install flathub md.obsidian.Obsidian
flatpak install flathub io.github.zen_browser.zen
flatpak install org.gtk.Gtk3theme.adw-gtk3 org.gtk.Gtk3theme.adw-gtk3-dark
```

## Use gtk themes for flatpaks

```bash
sudo flatpak override --filesystem=$HOME/.local/share/themes
sudo flatpak override --filesystem=$HOME/.local/share/icons
```

[Sway Atomic](https://fedoraproject.org/atomic-desktops/sway/)


