-- nvim config May 2024
-- set leader key
vim.g.mapleader = " "

local lazypath = vim.fn.stdpath "data" .. "/lazy/lazy.nvim"
if not vim.uv.fs_stat(lazypath) then
  vim.fn.system {
    "git",
    "clone",
    "--filter=blob:none",
    "https://github.com/folke/lazy.nvim.git",
    "--branch=stable",
    lazypath,
  }
end

-- Add lazy to the `runtimepath`, this allows us to `require` it.
---@diagnostic disable-next-line: undefined-field
vim.opt.rtp:prepend(lazypath)

-- Set up lazy, and load my `lua/plugins/` folder
require("lazy").setup({ import = "plugins" }, {
  change_detection = {
    notify = false,
  },
})

-- Sync clipboard between OS and Neovim.
vim.o.clipboard = 'unnamedplus'

-- Case-insensitive searching UNLESS \C or capital in search
vim.o.ignorecase = true
vim.o.smartcase = true

-- Set completeopt to have a better completion experience
vim.o.completeopt = 'menuone,noselect'

vim.o.laststatus = 3
vim.o.statusline = "%F Line:%l"

--use spaces instead of tabs, 2 of them
vim.o.shiftwidth = 2
vim.o.tabstop = 2
vim.o.expandtab = true
vim.bo.softtabstop = 2

vim.o.signcolumn = 'yes'
vim.o.updatetime = 520
vim.o.backup = false
vim.g.netrw_banner = false
vim.g.netrw_liststyle = 3
